# nf-core-sop


# log into eddie


start screen session on login node or reconnect to screen session

```bash
screen -S myrnaseqproj
```

Below are some instructions on how to get nf-core rnaseq pipeline running.
Also here is a link to how to use the screen command https://linuxize.com/post/how-to-use-linux-screen/



#cd to your project directory
 
 load java module
```bash
module load roslin/java/11.0.12
```

```bash
#install nextflow (only do this once)
wget -qO- https://get.nextflow.io | bash
 
# set envrionment variables
export NXF_TEMP=/exports/eddie/scratch/${USER}/tmp
export NXF_SINGULARITY_CACHEDIR=/exports/igmm/eddie/BioinformaticsResources/nfcore/singularity-images
export NXF_HOME=`pwd`/.nextflow
PATH =
```

Check nextflow runs
```bash
nextflow info
```

# nf-core

go to `nf-core/rnaseq` website click `launch button` and generate `nf-params.json` file
 
# example nextflow run command
```
./nextflow run nf-core/rnaseq -params-file nf-params.json -profile eddie -resume
```

# Checking run
```bash
nextflow log
nextflow log <session_name> -f name,hash,status,exit,cpus,memory
```

# Cleaning work directory

```bash
nextflow clean -h
```


## Useful links

* [Introduction to Eddie](https://www.wiki.ed.ac.uk/pages/viewpage.action?spaceKey=ResearchServices&title=Introduction+to+Eddie)

* [Bioinformatics Services wiki](https://www.wiki.ed.ac.uk/display/ResearchServices/Bioinformatics)